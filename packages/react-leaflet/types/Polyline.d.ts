import { PathProps } from '@react-leaflet/core';
import { LatLngExpression, PolylineOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface PolylineProps extends PolylineOptions, PathProps {
    children?: ReactNode;
    positions: LatLngExpression[] | LatLngExpression[][];
}
export declare const Polyline: any;
