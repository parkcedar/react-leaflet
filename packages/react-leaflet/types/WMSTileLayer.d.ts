import { LayerProps } from '@react-leaflet/core';
import { WMSOptions, WMSParams } from 'leaflet';
export interface WMSTileLayerProps extends WMSOptions, LayerProps {
    params?: WMSParams;
    url: string;
}
export declare const WMSTileLayer: any;
