import { EventedProps } from '@react-leaflet/core';
import { LayerOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface LayerGroupProps extends LayerOptions, EventedProps {
    children?: ReactNode;
}
export declare const LayerGroup: any;
