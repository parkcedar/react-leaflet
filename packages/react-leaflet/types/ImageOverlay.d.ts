import { MediaOverlayProps } from '@react-leaflet/core';
import { ReactNode } from 'react';
export interface ImageOverlayProps extends MediaOverlayProps {
    children?: ReactNode;
    url: string;
}
export declare const ImageOverlay: any;
