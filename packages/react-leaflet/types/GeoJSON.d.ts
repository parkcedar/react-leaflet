import { PathProps } from '@react-leaflet/core';
import { GeoJsonObject } from 'geojson';
import { GeoJSONOptions } from 'leaflet';
import { LayerGroupProps } from './LayerGroup';
export interface GeoJSONProps extends GeoJSONOptions, LayerGroupProps, PathProps {
    data: GeoJsonObject;
}
export declare const GeoJSON: any;
