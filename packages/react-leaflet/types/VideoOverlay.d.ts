import { MediaOverlayProps } from '@react-leaflet/core';
import { VideoOverlayOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface VideoOverlayProps extends MediaOverlayProps, VideoOverlayOptions {
    children?: ReactNode;
    play?: boolean;
    url: string | string[] | HTMLVideoElement;
}
export declare const VideoOverlay: any;
