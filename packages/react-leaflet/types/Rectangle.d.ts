import { PathProps } from '@react-leaflet/core';
import { LatLngBoundsExpression, PathOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface RectangleProps extends PathOptions, PathProps {
    bounds: LatLngBoundsExpression;
    children?: ReactNode;
}
export declare const Rectangle: any;
