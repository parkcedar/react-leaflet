import { EventedProps } from '@react-leaflet/core';
import { LatLngExpression, PopupOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface PopupProps extends PopupOptions, EventedProps {
    children?: ReactNode;
    onClose?: () => void;
    onOpen?: () => void;
    position?: LatLngExpression;
}
export declare const Popup: any;
