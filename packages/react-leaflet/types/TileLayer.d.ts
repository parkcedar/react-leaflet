import { LayerProps } from '@react-leaflet/core';
import { TileLayerOptions } from 'leaflet';
export interface TileLayerProps extends TileLayerOptions, LayerProps {
    url: string;
}
export declare const TileLayer: any;
