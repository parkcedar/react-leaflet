import { PathProps } from '@react-leaflet/core';
import { LayerGroupProps } from './LayerGroup';
export interface FeatureGroupProps extends LayerGroupProps, PathProps {
}
export declare const FeatureGroup: any;
