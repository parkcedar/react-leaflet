import { PathProps } from '@react-leaflet/core';
import { LatLngExpression, PolylineOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface PolygonProps extends PolylineOptions, PathProps {
    children?: ReactNode;
    positions: LatLngExpression[] | LatLngExpression[][] | LatLngExpression[][][];
}
export declare const Polygon: any;
