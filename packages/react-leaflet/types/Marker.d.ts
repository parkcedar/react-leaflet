import { EventedProps } from '@react-leaflet/core';
import { LatLngExpression, MarkerOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface MarkerProps extends MarkerOptions, EventedProps {
    children?: ReactNode;
    position: LatLngExpression;
}
export declare const Marker: any;
