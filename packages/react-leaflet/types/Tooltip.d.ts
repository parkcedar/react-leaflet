import { EventedProps } from '@react-leaflet/core';
import { LatLngExpression, TooltipOptions } from 'leaflet';
import { ReactNode } from 'react';
export interface TooltipProps extends TooltipOptions, EventedProps {
    children?: ReactNode;
    onClose?: () => void;
    onOpen?: () => void;
    position?: LatLngExpression;
}
export declare const Tooltip: any;
