import { MediaOverlayProps } from '@react-leaflet/core';
import { SVGOverlay as LeafletSVGOverlay } from 'leaflet';
import { ReactNode } from 'react';
export interface SVGOverlayProps extends MediaOverlayProps {
    attributes?: Record<string, string>;
    children?: ReactNode;
}
export declare const useSVGOverlayElement: any;
export declare const useSVGOverlay: any;
export declare const SVGOverlay: import("react").ForwardRefExoticComponent<SVGOverlayProps & import("react").RefAttributes<LeafletSVGOverlay>>;
