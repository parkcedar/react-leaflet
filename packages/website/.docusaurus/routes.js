
import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/blog/archive/',
    component: ComponentCreator('/blog/archive/','38a'),
    exact: true
  },
  {
    path: '/docs/',
    component: ComponentCreator('/docs/','a26'),
    routes: [
      {
        path: '/docs/api-components/',
        component: ComponentCreator('/docs/api-components/','b31'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/api-map/',
        component: ComponentCreator('/docs/api-map/','91a'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/core-api/',
        component: ComponentCreator('/docs/core-api/','9a5'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/core-architecture/',
        component: ComponentCreator('/docs/core-architecture/','a11'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/core-introduction/',
        component: ComponentCreator('/docs/core-introduction/','874'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-animated-panning/',
        component: ComponentCreator('/docs/example-animated-panning/','6f0'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-draggable-marker/',
        component: ComponentCreator('/docs/example-draggable-marker/','4cf'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-events/',
        component: ComponentCreator('/docs/example-events/','d7e'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-external-state/',
        component: ComponentCreator('/docs/example-external-state/','8cb'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-layers-control/',
        component: ComponentCreator('/docs/example-layers-control/','533'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-map-placeholder/',
        component: ComponentCreator('/docs/example-map-placeholder/','1bc'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-other-layers/',
        component: ComponentCreator('/docs/example-other-layers/','912'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-panes/',
        component: ComponentCreator('/docs/example-panes/','fe3'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-popup-marker/',
        component: ComponentCreator('/docs/example-popup-marker/','f16'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-react-control/',
        component: ComponentCreator('/docs/example-react-control/','caf'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-svg-overlay/',
        component: ComponentCreator('/docs/example-svg-overlay/','623'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-tooltips/',
        component: ComponentCreator('/docs/example-tooltips/','86a'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-vector-layers/',
        component: ComponentCreator('/docs/example-vector-layers/','d51'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/example-view-bounds/',
        component: ComponentCreator('/docs/example-view-bounds/','e1d'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/extra-logo-branding/',
        component: ComponentCreator('/docs/extra-logo-branding/','016'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/extra-plugins/',
        component: ComponentCreator('/docs/extra-plugins/','b44'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/logo-and-branding/',
        component: ComponentCreator('/docs/logo-and-branding/','b9d'),
        exact: true
      },
      {
        path: '/docs/start-installation/',
        component: ComponentCreator('/docs/start-installation/','8e9'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/start-introduction/',
        component: ComponentCreator('/docs/start-introduction/','a15'),
        exact: true,
        'sidebar': "docs"
      },
      {
        path: '/docs/start-setup/',
        component: ComponentCreator('/docs/start-setup/','528'),
        exact: true,
        'sidebar': "docs"
      }
    ]
  },
  {
    path: '/',
    component: ComponentCreator('/','deb'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*')
  }
];
