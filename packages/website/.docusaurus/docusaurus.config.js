export default {
  "title": "React Leaflet",
  "tagline": "React components for Leaflet maps",
  "url": "https://react-leaflet.js.org",
  "baseUrl": "/",
  "trailingSlash": true,
  "favicon": "img/logo.svg",
  "organizationName": "PaulLeCam",
  "projectName": "react-leaflet",
  "stylesheets": [
    "//unpkg.com/leaflet@1.7.1/dist/leaflet.css"
  ],
  "themeConfig": {
    "navbar": {
      "title": "React Leaflet",
      "logo": {
        "alt": "React Leaflet Logo",
        "src": "img/logo.svg"
      },
      "items": [
        {
          "to": "docs/start-introduction",
          "activeBasePath": "docs/start",
          "label": "Getting Started",
          "position": "left"
        },
        {
          "to": "docs/example-popup-marker",
          "activeBasePath": "docs/example",
          "label": "Examples",
          "position": "left"
        },
        {
          "to": "docs/api-map",
          "activeBasePath": "docs/api",
          "label": "API",
          "position": "left"
        },
        {
          "href": "https://github.com/PaulLeCam/react-leaflet",
          "label": "GitHub",
          "position": "right"
        }
      ],
      "hideOnScroll": false
    },
    "footer": {
      "style": "dark",
      "links": [
        {
          "title": "Docs",
          "items": [
            {
              "label": "Getting Started",
              "to": "docs/start-introduction"
            },
            {
              "label": "Public API",
              "to": "docs/api-map"
            },
            {
              "label": "Core API",
              "to": "docs/core-introduction"
            }
          ]
        },
        {
          "title": "Community",
          "items": [
            {
              "label": "Stack Overflow",
              "href": "https://stackoverflow.com/questions/tagged/react-leaflet"
            },
            {
              "label": "GitHub",
              "href": "https://github.com/PaulLeCam/react-leaflet"
            },
            {
              "label": "v2 (unsupported) documentation",
              "href": "https://react-leaflet-v2-docs.netlify.app/en/"
            }
          ]
        }
      ],
      "copyright": "Copyright © 2022 Paul Le Cam and contributors"
    },
    "prism": {
      "theme": {
        "plain": {
          "color": "#d6deeb",
          "backgroundColor": "#011627"
        },
        "styles": [
          {
            "types": [
              "changed"
            ],
            "style": {
              "color": "rgb(162, 191, 252)",
              "fontStyle": "italic"
            }
          },
          {
            "types": [
              "deleted"
            ],
            "style": {
              "color": "rgba(239, 83, 80, 0.56)",
              "fontStyle": "italic"
            }
          },
          {
            "types": [
              "inserted",
              "attr-name"
            ],
            "style": {
              "color": "rgb(173, 219, 103)",
              "fontStyle": "italic"
            }
          },
          {
            "types": [
              "comment"
            ],
            "style": {
              "color": "rgb(99, 119, 119)",
              "fontStyle": "italic"
            }
          },
          {
            "types": [
              "string",
              "url"
            ],
            "style": {
              "color": "rgb(173, 219, 103)"
            }
          },
          {
            "types": [
              "variable"
            ],
            "style": {
              "color": "rgb(214, 222, 235)"
            }
          },
          {
            "types": [
              "number"
            ],
            "style": {
              "color": "rgb(247, 140, 108)"
            }
          },
          {
            "types": [
              "builtin",
              "char",
              "constant",
              "function"
            ],
            "style": {
              "color": "rgb(130, 170, 255)"
            }
          },
          {
            "types": [
              "punctuation"
            ],
            "style": {
              "color": "rgb(199, 146, 234)"
            }
          },
          {
            "types": [
              "selector",
              "doctype"
            ],
            "style": {
              "color": "rgb(199, 146, 234)",
              "fontStyle": "italic"
            }
          },
          {
            "types": [
              "class-name"
            ],
            "style": {
              "color": "rgb(255, 203, 139)"
            }
          },
          {
            "types": [
              "tag",
              "operator",
              "keyword"
            ],
            "style": {
              "color": "rgb(127, 219, 202)"
            }
          },
          {
            "types": [
              "boolean"
            ],
            "style": {
              "color": "rgb(255, 88, 116)"
            }
          },
          {
            "types": [
              "property"
            ],
            "style": {
              "color": "rgb(128, 203, 196)"
            }
          },
          {
            "types": [
              "namespace"
            ],
            "style": {
              "color": "rgb(178, 204, 214)"
            }
          }
        ]
      },
      "additionalLanguages": []
    },
    "colorMode": {
      "defaultMode": "light",
      "disableSwitch": false,
      "respectPrefersColorScheme": false,
      "switchConfig": {
        "darkIcon": "🌜",
        "darkIconStyle": {},
        "lightIcon": "🌞",
        "lightIconStyle": {}
      }
    },
    "docs": {
      "versionPersistence": "localStorage"
    },
    "metadata": [],
    "hideableSidebar": false,
    "tableOfContents": {
      "minHeadingLevel": 2,
      "maxHeadingLevel": 3
    },
    "liveCodeBlock": {
      "playgroundPosition": "bottom"
    }
  },
  "presets": [
    [
      "@docusaurus/preset-classic",
      {
        "docs": {
          "sidebarPath": "/Users/lachlan/Developer/react-leaflet/packages/website/sidebars.js"
        },
        "theme": {
          "customCss": "/Users/lachlan/Developer/react-leaflet/packages/website/src/css/custom.css"
        }
      }
    ]
  ],
  "plugins": [
    "@react-leaflet/docusaurus-plugin"
  ],
  "themes": [
    "@docusaurus/theme-live-codeblock"
  ],
  "baseUrlIssueBanner": true,
  "i18n": {
    "defaultLocale": "en",
    "locales": [
      "en"
    ],
    "localeConfigs": {}
  },
  "onBrokenLinks": "throw",
  "onBrokenMarkdownLinks": "warn",
  "onDuplicateRoutes": "warn",
  "staticDirectories": [
    "static"
  ],
  "customFields": {},
  "titleDelimiter": "|",
  "noIndex": false
};